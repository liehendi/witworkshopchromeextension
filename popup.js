function writeShoppingItems() { //write all current shopping items into the popup.html

  // clear both cart and total value
  $("#cart").empty();
  $("#total").empty();

  // write table header
  var header = [
    "<tr>",
    "<th>Label</th>",
    "<th>Price</th>",
    "<th>Webpage</th>",
    "<th>Action</th>",
    "</tr>"
  ]
  $("#cart").append(header.join())

  // get all shopping cart items under the key 'shopping_cart'
  // chrome storage is async, which means we need to only write the items after the call back is finished
  chrome.storage.sync.get('shopping_cart', function(obj) {
    var items = obj['shopping_cart'];
    var sum = 0;

    for (x in items) {
      item = items[x];

      var bigstring = [
        '<tr>',
        '<td>' + item['label'] + '</td>',
        '<td>$' + item['price'] + '</td>',
        '<td><a href="' + item['url'] + '"">Link</a></td>',
        '<td><a href="#">Delete</a></td>',
        '</tr>',
      ]

      $("#cart").append(bigstring.join());
      sum += parseInt(item['price']);
    }

    $("#total").append(sum);
  });
}

function addShoppingItem() {
  // get the current tab's URL
  var url = "";
  chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
    url = tabs[0].url;
    var label = $("#label").val();
    var price = $("#price").val();

    item = {
      'url': url,
      'label': label,
      'price': price
    };

    // grab the storage items, update them locally, then write them into the storage again
    chrome.storage.sync.get('shopping_cart', function(obj) {
      var items = obj['shopping_cart'];

      items.push(item);
      chrome.storage.sync.set({'shopping_cart': items});

      // refresh the item view
      writeShoppingItems();
    });
  });
}

function deleteShoppingItem() {
  // Place holder, haven't wrote them down yet
}

// dump everything when the browser is opened
document.addEventListener('DOMContentLoaded', function () {
  $("#addbutton").click(addShoppingItem);
  writeShoppingItems();
});
